﻿using Models.ServiceLayer;
using Models.ServiceLayer.BusinessPartner;
using Models.ServiceLayer.Countries;
using Models.ServiceLayer.IncomingPayment;
using Models.ServiceLayer.Invoices;
using Models.ServiceLayer.Item;
using Models.ServiceLayer.Orders;
using Models.ServiceLayer.Payments;
using Models.ServiceLayer.PredefinedText;
using Models.ServiceLayer.Shippings;
using Models.ServiceLayer.States;

namespace Infrastructure.Clients.ServiceLayer
{
    public class ServiceLayerResources : VirtualResources
        {
            public ServiceLayerResources()
            {
                ResourcesUrl.Add(typeof(ServiceLayerLoginModel), LoginUrl);
                ResourcesUrl.Add(typeof(SAPOrderModel), OrdersUrl);
                ResourcesUrl.Add(typeof(BusinessPartnerModel), BusinessPartnerUrl);
                ResourcesUrl.Add(typeof(StateModel), StateUrl);
                ResourcesUrl.Add(typeof(PaymentTermsTypesModel), PaymentTermsTypesUrl);
                ResourcesUrl.Add(typeof(WizardPaymentMethodsModel), WizardPaymentMethodsUrl);
                ResourcesUrl.Add(typeof(ItemModel), ItemUrl);
                ResourcesUrl.Add(typeof(CountryModel), CountryUrl);
                ResourcesUrl.Add(typeof(METrackingModel), METrackingUrl);
                ResourcesUrl.Add(typeof(InvoiceModel), InvoiceUrl);
                ResourcesUrl.Add(typeof(IncomingPaymentModel), IncomingPaymentUrl);
                ResourcesUrl.Add(typeof(PredefinedTextModel), PredefinedTextModel);
            }

            public const string LoginUrl = "Login";
            private const string BusinessPartnerUrl = "BusinessPartners";
            private const string OrdersUrl = "Orders";
            private const string StateUrl = "States";
            private const string PaymentTermsTypesUrl = "PaymentTermsTypes";
            private const string WizardPaymentMethodsUrl = "WizardPaymentMethods";
            private const string ItemUrl = "Items";
            private const string CountryUrl = "Countries";
            private const string METrackingUrl = "U_BCE_TRCK";
            private const string InvoiceUrl = "Invoices";
            private const string IncomingPaymentUrl = "IncomingPayments";
            private const string PredefinedTextModel = "PredefinedTexts";
        }
    }
