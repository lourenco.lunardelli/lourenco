using Models;

namespace Infrastructure.Clients.Bahngleis
{
    public class BahngleisResources : VirtualResources
    {
        public BahngleisResources()
        {
            ResourcesUrl.Add(typeof(NewOrderModel), OrderUrl);
        }
        
        private const string OrderUrl = "Order";
        
    }
}