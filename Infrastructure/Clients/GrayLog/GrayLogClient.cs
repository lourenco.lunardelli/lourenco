﻿using Serilog;
using Serilog.Sinks.Graylog;
using System;

namespace Infrastructure.Clients.GrayLog
{
    public static class GrayLogClient
    {
        private static ILogger _logClient;

        public static ILogger GetLogger()
        {
            if (_logClient == null)
            {
                var options = new GraylogSinkOptions
                {
                    HostnameOrAddress = Environment.GetEnvironmentVariable("GRAYLOG_ADDRESS"),
                    Port = int.Parse(Environment.GetEnvironmentVariable("GRAYLOG_PORT")),
                    Facility = Environment.GetEnvironmentVariable("GRAYLOG_FACILITY")
                };

                _logClient = new LoggerConfiguration()
                .WriteTo.Sink(new GraylogSink(options))
                .CreateLogger();
            }

            return _logClient;
        }
    }
}