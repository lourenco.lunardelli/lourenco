﻿using Flurl.Http;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Clients.TokenKeeper
{
    public class TokenKeeperClient
    {
        private FlurlClient _client;
        private TokenKeeperParams _tokenParams;
        private Token _token;

        public TokenKeeperClient(TokenKeeperParams tokenParams)
        {
            _client = new FlurlClient(Environment.GetEnvironmentVariable("TOKENKEEPER_ADDRESS"));
            _tokenParams = tokenParams;
        }

        public async Task<Token> GetRawToken()
        {
            if (_token == null || DateTimeOffset.UtcNow.ToUnixTimeSeconds() > _token.ExpireTime)
            {
                Token token = await _client.Request()
                    .WithHeader("x-api-key", _tokenParams.ApiKey)
                    .WithHeader("client_id", _tokenParams.ClientId)
                    .GetJsonAsync<Token>();
    
                if (token.AccessToken is null)
                {
                    return null;
                }
    
                _token = token;
                
                _token.ExpireTime = DateTimeOffset.UtcNow.AddSeconds(_token.ExpiresIn).ToUnixTimeSeconds();
            }

            return _token;
        }
        
        public async Task<string> GetBearerToken()
        {
            await GetRawToken();
            
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_token.AccessToken));

            var secToken = new JwtSecurityToken(
                signingCredentials: new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256),
                issuer: "Bahn",
                audience: "Bahn",
                claims: new[]
                {
                    new Claim(JwtRegisteredClaimNames.Sub, "Nuuk")
                },
                expires: DateTime.UtcNow.AddDays(1));

            var handler = new JwtSecurityTokenHandler();
            return handler.WriteToken(secToken);
        }
    }
}
