using Flurl.Http;
using Models.Roter;
using System;
using System.Threading.Tasks;

namespace Infrastructure.Clients.Roter
{
    public class RoterClient : VirtualClient
    {
        private static DateTime _expiry = DateTime.MinValue;
        private static string _token;

        public RoterClient() : base(Environment.GetEnvironmentVariable("WMS_APIURL"), new RoterResources())
        {
        }


        public static bool HasExpired
        {
            get { return (_expiry < DateTime.Now); }
        }

        /// <summary>
        /// Checks if the API session is currently valid and authenticates if necessary.
        /// </summary>
        public async Task Connect()
        {
            var loginTimeout = new TimeSpan(0, 0, 10);
            var loginResult = await _client.BaseUrl.WithTimeout(loginTimeout)
                .AppendPathSegment("login")
                .PostJsonAsync(new
                {
                    userID = Environment.GetEnvironmentVariable("WMS_APIUSER"),
                    AccessKey = Environment.GetEnvironmentVariable("WMS_APIPASSW")
                })
                .ReceiveJson<LoginModel>();

            _expiry = loginResult.expiration.AddMinutes(-1);
            _token = loginResult.accessToken;
        }

        public override async Task HandleBeforeCallAsync(HttpCall call)
        {
            if (HasExpired)
                await Connect();

            call.FlurlRequest.WithHeader("Authorization", $"Bearer {_token}");
        }

        public override async Task HandleAfterCallAsync(HttpCall call) { }
    }
}