﻿using System.Net.Http;
using Flurl.Http.Configuration;

namespace Infrastructure.Clients
{
    /// <summary>
    /// Custom HttpClient factory class to ignore certificate errors when using Flurl.
    /// </summary>
    class CustomHttpClientFactory : DefaultHttpClientFactory
    {
        public override HttpMessageHandler CreateMessageHandler()
        {
            return new HttpClientHandler
            {
                ServerCertificateCustomValidationCallback = delegate { return true; }
            };
        }
    }
}