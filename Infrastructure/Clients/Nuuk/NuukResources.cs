using Infrastructure.Nuuk;

namespace Infrastructure.Clients.Nuuk
{
    public class NuukResources : VirtualResources
    {
        public NuukResources()
        {
            ResourcesUrl.Add(typeof(NuukCnpjModel), ConsultaCadastroUrl);
        }
        
        public const string ConsultaCadastroUrl = "Nuuk.Service/Consulta/ConsultaCNPJ";   
    }
}