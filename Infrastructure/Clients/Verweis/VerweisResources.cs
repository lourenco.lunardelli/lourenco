﻿using Models.Verweis;

namespace Infrastructure.Clients.Verweis
{
    public class VerweisResources : VirtualResources
    {
        public VerweisResources()
        {
            ResourcesUrl.Add(typeof(NewCompanyModel), Company);
        }

        private const string Company = "Company";
    }
}
