﻿using Flurl.Http;
using System;
using System.Threading.Tasks;

namespace Infrastructure.Clients.Verweis
{
    public class VerweisClient : VirtualClient
    {
        public VerweisClient() : base(Environment.GetEnvironmentVariable("VERWEIS_ADDRESS"), new VerweisResources())
        {
            _client.Configure(settings => settings.HttpClientFactory = new CustomHttpClientFactory());
        }

        public override async Task HandleBeforeCallAsync(HttpCall call) { }

        public override async Task HandleAfterCallAsync(HttpCall call) { }
    }
}
