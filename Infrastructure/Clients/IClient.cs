using Flurl.Http;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Infrastructure.Clients
{
    public interface IClient
    {
        Task <List<T>> GetAsync<T>(object query = null, string path = null, Dictionary<string, string> header = null);
        Task<T> GetByIdAsync<T>(object entityId, string path = null);
        Task<byte[]> GetAsyncNotDeserialized(object entityId, string path = null);
        Task<T> GetByQueryAsync<T>(object query = null, string path = null);


        Task<T> PostAsync<T>(T entity, string path = null);
        Task<string> PostAsyncNotDeserialized<T>(T entity, string path = null);
        Task<T> PostByQueryAsync<T>(T entity, string path = null);

        Task<T> PatchAsync<T>(object entityId, T entity, string path = null);
        Task<T> PutAsync<T>(object entityId, T entity, string path = null, object query = null);

        Task HandleErrorAsync(HttpCall call);
        Task HandleBeforeCallAsync(HttpCall call);
        Task HandleAfterCallAsync(HttpCall call);
    }
}