﻿using Domain.Bahngleis;
using Domain.BusinessPartner.Interfaces;
using Domain.Documents;
using Domain.Nuuk;
using Flurl.Http;
using Flurl.Http.Configuration;
using Hangfire;
using Hangfire.Server;
using Infrastructure;
using Infrastructure.Facades;
using Infrastructure.Nuuk;
using Infrastructure.ServiceLayer.Models.BusinessPartner;
using Infrastructure.ServiceLayer.Models.Contacts;
using Infrastructure.ServiceLayer.Models.Countries;
using legacyModels.Models;
using Models;
using Models.ServiceLayer.BusinessPartner;
using Models.ServiceLayer.Contacts;
using Models.ServiceLayer.Counties;
using Models.Verweis;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Domain.BusinessPartner
{
    public class BusinessPartnerService : InjectionBase, IBusinessPartnerService
    {
        public BusinessPartnerService(IServiceProvider serviceProvider) : base(serviceProvider)
        {
            FlurlHttp.Configure(c =>
            {
                // Sets up the custom factory class in OrderModel to ignore certificate errors in Flurl requests
                c.CookiesEnabled = true;
                c.JsonSerializer = new NewtonsoftJsonSerializer(new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
            });

            ServicePointManager.Expect100Continue = false;
            ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
        }

        [Queue("business_partner")]
        [AutomaticRetry(OnAttemptsExceeded = AttemptsExceededAction.Delete, DelaysInSeconds = new[] { 60 })]
        public async Task UpsertBusinessPartner(string OrderNumber, string SiteName, PerformContext context)
        {
            int retryCount = context.GetJobParameter<int>("RetryCount");
            OrderModel orderModel = await OrderFacade.BahngleisRepo.GetOrderModelAsync(OrderNumber, SiteName);
            try
            {
                if (DocumentsSpecification.ShouldIntegrate(orderModel, OrderFacade.MageRepo))
                {
                    Treatment(orderModel);

                    #region MAPPING
                    //------------------------------LISTS------------------------------
                    BusinessPartnerModel businessPartner = await BusinessPartnerFacade.Repo.GetBusinessPartner(orderModel.Customer.TaxVat);

                    businessPartner.EmailAddress = orderModel.Customer.Email;
                    businessPartner.Password = orderModel.Customer.TaxVat;
                    businessPartner.GroupCode = BusinessPartnerSpecification.IsTaxPayer(orderModel.Customer) ? BusinessPartnerHelper._taxPayerGroupCode : BusinessPartnerHelper._finalCustomerGroupCode;

                    Fill(orderModel, businessPartner);

                    BusinessPartnerHelper.CustomerTreatment(orderModel.Customer, businessPartner);

                    string BPName = !string.IsNullOrEmpty(orderModel.Customer.LastName) ? $"{orderModel.Customer.FirstName.Trim()} {orderModel.Customer.LastName.Trim()}" : orderModel.Customer.FirstName.Trim();
                    businessPartner.CardName = BPName.Length > 40 ? BPName.Substring(0, 40) : BPName;
                    if (!string.IsNullOrEmpty(orderModel.Customer.CompanyName))
                        businessPartner.CardName = orderModel.Customer.CompanyName;

                    businessPartner.Cellular = orderModel.Customer.MobileNumber;

                    if (!string.IsNullOrEmpty(orderModel.Customer.PhoneNumber) && orderModel.Customer.PhoneNumber.Length >= 8)
                    {
                        businessPartner.Phone1 = orderModel.Customer.PhoneNumber.Length > 12 ? orderModel.Customer.PhoneNumber.Substring(2, 10) : orderModel.Customer.PhoneNumber.Substring(2, orderModel.Customer.PhoneNumber.Length - 2);
                        businessPartner.Phone2 = orderModel.Customer.PhoneNumber.Substring(0, 2);
                    }

                    businessPartner.AliasName = orderModel.Customer.Nickname;

                    //------------------------------USER FIELDS------------------------------
                    businessPartner.U_IE_UltimaBusca = DateTime.Today;
                    businessPartner.U_ERP_MagCode = orderModel.Customer.CustomerID.ToString();

                    if (orderModel.Channel.Equals("Loja virtual"))
                        businessPartner.U_ERP_LISTA_PN = "01";

                    else if (orderModel.Channel.ToLower().Equals("mercadolivre"))
                        businessPartner.U_ERP_LISTA_PN = "02";

                    #endregion

                    //------------------------------SEND TO SAP------------------------------
                    if (businessPartner.CardCode is null)
                        businessPartner = await BusinessPartnerFacade.Repo.AddBusinessPartner(businessPartner);
                    else
                        await BusinessPartnerFacade.Repo.UpdateBusinessPartner(businessPartner);

                    OrderFacade.BahngleisService.UpdateDocuments(orderModel);
                    orderModel.LastJobId = BackgroundJob.Enqueue(() => OrderFacade.SapService.AddOrder(OrderNumber, SiteName, null));
                }
                else
                {
                    orderModel.EcommerceStatus = "canceled";
                    OrderFacade.BahngleisService.UpdateDocuments(orderModel);
                }
            }
            catch (Exception exc)
            {
                Logger.GetLogger().Error(exc, "SAP - Error to update business partner", orderModel);

                if (orderModel.Ecommerce.Platform.Name.ToUpper().Equals("MAGENTO") && retryCount == 10)
                {
                    orderModel.EcommerceStatus = "holded";
                    OrderFacade.MageService.HoldOrder(orderModel.Ecommerce, orderModel.OrderNumber, "Erro na integração com SAP: " + exc.Message, false);
                }
                else
                    orderModel.EcommerceStatus = "error";

                OrderFacade.BahngleisService.UpdateDocuments(orderModel);

                throw exc;
            }
        }


        //---------------------------------------------FILL LISTS---------------------------------------------
        public async Task Fill(NewOrderModel orderModel, BusinessPartnerModel businessPartner)
        {
            if (BusinessPartnerSpecification.IsCNPJ(orderModel.Customer.TaxIdentification))
                FillStateRegister(orderModel.Customer, orderModel.Payment.Address);

            FillFiscalData(businessPartner, orderModel.Customer);

            if (BusinessPartnerSpecification.IsB2B(orderModel))
                await FillB2BFields(orderModel, businessPartner);

            businessPartner.BPAddresses.Clear();
            FillAddress(businessPartner, orderModel.Payment.Address);
            FillAddress(businessPartner, orderModel.Shipping.Address);

            await FillPayment(businessPartner, orderModel.Payment);
        }

        public void FillStateRegister(NewCustomerModel customer, NewAddressModel billingAddress)
        {
            NuukCnpjModel taxITResponse = NuukFacade.Service.FindRegistry(billingAddress.UF, BusinessPartnerHelper.Unformat(customer.TaxIdentification)).Result;

            if (NuukSpecification.Found(taxITResponse))
            {
                var consultaCadastro = taxITResponse.Consulta;

                customer.ResselerInfo.CorporateName = consultaCadastro.RazaoSocial ?? customer.ResselerInfo.CorporateName;
                customer.ResselerInfo.StateRegistry = (string.IsNullOrEmpty(consultaCadastro.InscricaoEstadual) || consultaCadastro.SituacaoIE.ToLower() == "inativo") ? "Isento" : consultaCadastro.InscricaoEstadual.Trim().ToLower();
                //businessPartner.U_IE_UltimaBusca = DateTime.Today;           
            }
            else
                customer.ResselerInfo.StateRegistry = "Isento";
        }

        public void FillFiscalData(BusinessPartnerModel businessPartner, NewCustomerModel customer)
        {
            // Should we make a put request to clear this field before add ?
            businessPartner.BPFiscalTaxIDCollection.Clear();


            if (BusinessPartnerSpecification.IsCNPJ(customer.TaxIdentification))
            {
                businessPartner.BPFiscalTaxIDCollection.Add(new BusinessPartnerFiscalTaxIdCollection
                {
                    Address = "",
                    BPCode = businessPartner.CardCode,
                    TaxId0 = customer.TaxIdentification,
                    TaxId1 = string.IsNullOrEmpty(customer.ResselerInfo.StateRegistry) ? null : customer.ResselerInfo.StateRegistry,
                    AddrType = "bo_ShipTo"
                });

                businessPartner.BPFiscalTaxIDCollection.Add(new BusinessPartnerFiscalTaxIdCollection
                {
                    Address = "ENTREGA",
                    BPCode = businessPartner.CardCode,
                    TaxId0 = customer.TaxIdentification,
                    TaxId1 = string.IsNullOrEmpty(customer.ResselerInfo.StateRegistry) ? null : customer.ResselerInfo.StateRegistry,
                    AddrType = "bo_ShipTo"
                });

                businessPartner.BPFiscalTaxIDCollection.Add(new BusinessPartnerFiscalTaxIdCollection
                {
                    Address = "PAGAMENTO",
                    BPCode = businessPartner.CardCode,
                    TaxId0 = customer.TaxIdentification,
                    TaxId1 = string.IsNullOrEmpty(customer.ResselerInfo.StateRegistry) ? null : customer.ResselerInfo.StateRegistry,
                    AddrType = "bo_BillTo"
                });
            }
            else
            {
                businessPartner.BPFiscalTaxIDCollection.Add(new BusinessPartnerFiscalTaxIdCollection
                {
                    Address = "",
                    BPCode = businessPartner.CardCode,
                    TaxId4 = customer.TaxIdentification,
                    AddrType = "bo_ShipTo"
                });

                businessPartner.BPFiscalTaxIDCollection.Add(new BusinessPartnerFiscalTaxIdCollection
                {
                    Address = "ENTREGA",
                    BPCode = businessPartner.CardCode,
                    TaxId4 = customer.TaxIdentification,
                    AddrType = "bo_ShipTo"
                });

                businessPartner.BPFiscalTaxIDCollection.Add(new BusinessPartnerFiscalTaxIdCollection
                {
                    Address = "PAGAMENTO",
                    BPCode = businessPartner.CardCode,
                    TaxId4 = customer.TaxIdentification,
                    AddrType = "bo_BillTo"
                });
            }
        }

        public async Task FillB2BFields(NewOrderModel orderModel, BusinessPartnerModel businessPartner)
        {
            NewEcommerceModel ecommerceModel = await VerweisFacade.Repo.GetEcommerce(order.EcommerceID);

            if (ecommerceModel.Platform.Name.ToUpper().Equals("BRINGIT") && BusinessPartnerSpecification.IsCNPJ(orderModel.Customer.TaxIdentification))
            {
                var mageCustomerInfo = OrderFacade.MageRepo.customerCustomerInfoAsync(orderModel);

                if (mageCustomerInfo.b2b != null && !string.IsNullOrEmpty(mageCustomerInfo.b2b.revenda_sede_propria))
                {
                    #region Business partner additional info

                    businessPartner.U_BCEEmployeesNo = string.IsNullOrEmpty(mageCustomerInfo.b2b.revenda_n_de_funcionarios) ? string.Empty : mageCustomerInfo.b2b.revenda_n_de_funcionarios;
                    businessPartner.U_BCEWebSite = string.IsNullOrEmpty(mageCustomerInfo.b2b.revenda_site) ? string.Empty : mageCustomerInfo.b2b.revenda_site;
                    businessPartner.U_BCEOwnHeadquarter = mageCustomerInfo.b2b.revenda_sede_propria == "1" ? "Y" : "N";
                    businessPartner.U_BCEFoundationDate = Convert.ToDateTime(mageCustomerInfo.b2b.revenda_company_foundation_date);
                    businessPartner.U_BCEAbout = string.IsNullOrEmpty(mageCustomerInfo.b2b.revenda_business_description) ? string.Empty : mageCustomerInfo.b2b.revenda_business_description;

                    #endregion Business partner additional info

                    #region Contact persons

                    if (!string.IsNullOrEmpty(mageCustomerInfo.b2b.socio1_nome_completo) && mageCustomerInfo.b2b.socio1_nome_completo != "\n")
                    {
                        var internalCode = businessPartner.ContactEmployees.Where(s => s.Name == "Sócio 1").Select(s => s.InternalCode).FirstOrDefault();

                        if (businessPartner.ContactEmployees.Any(s => s.Name == "Sócio 1"))
                            businessPartner.ContactEmployees.Remove(businessPartner.ContactEmployees.Where(s => s.Name == "Sócio 1").First());

                        businessPartner.ContactEmployees.Add(new ContactsModel
                        {
                            Name = "Sócio 1",
                            Active = "tYES",
                            FirstName = mageCustomerInfo.b2b.socio1_nome_completo.Replace("\n", ""),
                            E_Mail = mageCustomerInfo.b2b.socio1_email.Replace("\n", ""),
                            Phone1 = mageCustomerInfo.b2b.socio1_telefone.Replace("\n", ""),
                            U_BCECPF = mageCustomerInfo.b2b.socio1_cpf.Replace("\n", ""),
                            U_BCESkype = mageCustomerInfo.b2b.socio1_skype.Replace("\n", ""),
                            U_BCEPodeCotar = mageCustomerInfo.b2b.socio1_pode_cotar == "1" ? "Y" : "N",
                            U_BCEPodeComprar = mageCustomerInfo.b2b.socio1_pode_comprar == "1" ? "Y" : "N",
                            U_BCEPodeRetirar = mageCustomerInfo.b2b.socio1_pode_retirar == "1" ? "Y" : "N",
                            InternalCode = internalCode
                        });
                    }

                    if (!string.IsNullOrEmpty(mageCustomerInfo.b2b.socio2_nome_completo) && mageCustomerInfo.b2b.socio2_nome_completo != "\n")
                    {
                        var internalCode = businessPartner.ContactEmployees.Where(s => s.Name == "Sócio 1").Select(s => s.InternalCode).FirstOrDefault();

                        if (businessPartner.ContactEmployees.Any(s => s.Name == "Sócio 2"))
                            businessPartner.ContactEmployees.Remove(businessPartner.ContactEmployees.Where(s => s.Name == "Sócio 2").First());

                        businessPartner.ContactEmployees.Add(new ContactsModel
                        {
                            Name = "Sócio 2",
                            Active = "tYES",
                            FirstName = mageCustomerInfo.b2b.socio2_nome_completo.Replace("\n", ""),
                            E_Mail = mageCustomerInfo.b2b.socio2_email.Replace("\n", ""),
                            Phone1 = mageCustomerInfo.b2b.socio2_telefone.Replace("\n", ""),
                            U_BCECPF = mageCustomerInfo.b2b.socio2_cpf.Replace("\n", ""),
                            U_BCESkype = mageCustomerInfo.b2b.socio2_skype.Replace("\n", ""),
                            U_BCEPodeCotar = mageCustomerInfo.b2b.socio2_pode_cotar == "1" ? "Y" : "N",
                            U_BCEPodeComprar = mageCustomerInfo.b2b.socio2_pode_comprar == "1" ? "Y" : "N",
                            U_BCEPodeRetirar = mageCustomerInfo.b2b.socio2_pode_retirar == "1" ? "Y" : "N",
                            InternalCode = internalCode
                        });
                    }

                    if (!string.IsNullOrEmpty(mageCustomerInfo.b2b.financeiro_nome_completo) && mageCustomerInfo.b2b.financeiro_nome_completo != "\n")
                    {
                        var internalCode = businessPartner.ContactEmployees.Where(s => s.Name == "Sócio 1").Select(s => s.InternalCode).FirstOrDefault();

                        if (businessPartner.ContactEmployees.Any(s => s.Name == "Financeiro"))
                            businessPartner.ContactEmployees.Remove(businessPartner.ContactEmployees.Where(s => s.Name == "Financeiro").First());

                        businessPartner.ContactEmployees.Add(new ContactsModel
                        {
                            Name = "Financeiro",
                            Active = "tYES",
                            FirstName = mageCustomerInfo.b2b.financeiro_nome_completo.Replace("\n", ""),
                            E_Mail = mageCustomerInfo.b2b.financeiro_email.Replace("\n", ""),
                            Phone1 = mageCustomerInfo.b2b.financeiro_telefone.Replace("\n", ""),
                            U_BCECPF = mageCustomerInfo.b2b.financeiro_cpf.Replace("\n", ""),
                            U_BCESkype = mageCustomerInfo.b2b.financeiro_skype.Replace("\n", ""),
                            U_BCEPodeCotar = mageCustomerInfo.b2b.financeiro_pode_cotar == "1" ? "Y" : "N",
                            U_BCEPodeComprar = mageCustomerInfo.b2b.financeiro_pode_comprar == "1" ? "Y" : "N",
                            U_BCEPodeRetirar = mageCustomerInfo.b2b.financeiro_pode_retirar == "1" ? "Y" : "N",
                            InternalCode = internalCode
                        });
                    }

                    if (!string.IsNullOrEmpty(mageCustomerInfo.b2b.comprador_nome_completo) && mageCustomerInfo.b2b.comprador_nome_completo != "\n")
                    {
                        var internalCode = businessPartner.ContactEmployees.Where(s => s.Name == "Sócio 1").Select(s => s.InternalCode).FirstOrDefault();

                        if (businessPartner.ContactEmployees.Any(s => s.Name == "Comprador"))
                            businessPartner.ContactEmployees.Remove(businessPartner.ContactEmployees.Where(s => s.Name == "Comprador").First());

                        businessPartner.ContactEmployees.Add(new ContactsModel
                        {
                            Name = "Comprador",
                            Active = "tYES",
                            FirstName = mageCustomerInfo.b2b.comprador_nome_completo.Replace("\n", ""),
                            E_Mail = mageCustomerInfo.b2b.comprador_email.Replace("\n", ""),
                            Phone1 = mageCustomerInfo.b2b.comprador_telefone.Replace("\n", ""),
                            U_BCECPF = mageCustomerInfo.b2b.comprador_cpf.Replace("\n", ""),
                            U_BCESkype = mageCustomerInfo.b2b.comprador_skype.Replace("\n", ""),
                            U_BCEPodeCotar = mageCustomerInfo.b2b.comprador_pode_cotar == "1" ? "Y" : "N",
                            U_BCEPodeComprar = mageCustomerInfo.b2b.comprador_pode_comprar == "1" ? "Y" : "N",
                            U_BCEPodeRetirar = mageCustomerInfo.b2b.comprador_pode_retirar == "1" ? "Y" : "N",
                            InternalCode = internalCode
                        });
                    }

                    #endregion Contact persons
                }
            }
        }

        public void FillAddress(BusinessPartnerModel businessPartner, NewAddressModel address)
        {
            // IE indicator for Tripple One (Skill) fiscal add-on
            var ieIndicator = BusinessPartnerSpecification.IsTaxPayer(businessPartner) ? BusinessPartnerHelper._taxPayerIndicator : BusinessPartnerHelper._notTaxPayerIndicator;

            var county = GetCounty(address.UF, address.City, address.ZipCode);
            businessPartner.BPAddresses.Add(BusinessPartnerHelper.ParseAddress(address, businessPartner.CardCode, ieIndicator, county));
        }

        public async Task FillPayment(BusinessPartnerModel businessPartner, NewPaymentModel payment)
        {
            EcommerceMethod ecommerceMethod = await VerweisFacade.Repo.GetMethod(payment.PaymentMethodID, null);

            if (ecommerceMethod != null)
            {
                if (BusinessPartnerSpecification.ShouldAddPaymentMethod(businessPartner.BPPaymentMethods, ecommerceMethod.ERPName))
                {
                    businessPartner.BPPaymentMethods.Add(new BusinessPartnerPaymentMethods
                    {
                        PaymentMethodCode = ecommerceMethod.ERPName
                    });
                }
                businessPartner.PeymentMethodCode = ecommerceMethod.ERPName;
            }
            else
                businessPartner.PeymentMethodCode = "";
        }


        public void Treatment(NewOrderModel orderModel)
        {
            //------------------------------MODEL TREATMENT BAHNGLEIS------------------------------
            AddressTreatment.Treatment(orderModel.Customer.Address, "Customer");
            AddressTreatment.Treatment(orderModel.Shipping.Address, "Entrega");
            AddressTreatment.Treatment(orderModel.Payment.Address, "Pagamento");

            CustomerTreatment.Treatment(orderModel.Customer, orderModel.Shipping.Address);

            PaymentTreatment.Treatment(orderModel.Payment, orderModel.Channel, orderModel.ChannelOrderID, orderModel.OrderNumber);

            ProductTreatment.Treatment(orderModel.Products);

            OrderTreatment.Treatment(orderModel);
        }


        //---------------------------------------------AUXILIARIES---------------------------------------------
        public string GetCounty(NewAddressModel address)
        {
            //--------------------SAP--------------------
            CountyModel county = ServiceLayerXS.HttpClient.GetAsync<CountyModel>($"?$filter=Name eq '{address.City.Replace("'", "").ToUpper()}' and State eq '{address.State.ToUpper()}'").Result.FirstOrDefault();

            if (county == null)
                county = ServiceLayerXS.HttpClient.GetAsync<CountyModel>($"?$filter=Name eq '{BusinessPartnerHelper.RemoveAccents(address.City).Replace("'", "").ToUpper()}' and State eq '{address.State.ToUpper()}'").Result.FirstOrDefault();

            if (county == null && address.City.Contains("("))
                county = ServiceLayerXS.HttpClient.GetAsync<CountyModel>($"?$filter=Name eq '{address.City.Split('(', ')')[1].ToUpper()}' and State eq '{address.State.ToUpper()}'").Result.FirstOrDefault();

            if (county == null && !string.IsNullOrEmpty(address.IbgeCode))
                county = ServiceLayerXS.HttpClient.GetAsync<CountyModel>($"?$filter=IbgeCode eq '{address.IbgeCode}'").Result.FirstOrDefault();

            //--------------------VIA CEP--------------------
            if (county == null)
            {
                var result = OrderFacade.ShippingRepo.GetAddressViaCEP(address.ZipCode);

                if (!string.IsNullOrEmpty(result.localidade))
                {
                    if (county == null && !string.IsNullOrEmpty(result.ibge))
                        county = ServiceLayerXS.HttpClient.GetAsync<CountyModel>($"?$filter=IbgeCode eq '{result.ibge}'").Result.FirstOrDefault();

                    if (county == null)
                        county = ServiceLayerXS.HttpClient.GetAsync<CountyModel>($"?$filter=Name eq '{result.localidade.ToUpper()}' and State eq '{result.uf.ToUpper()}'").Result.FirstOrDefault();

                    if (county == null)
                        county = ServiceLayerXS.HttpClient.GetAsync<CountyModel>($"?$filter=Name eq '{BusinessPartnerHelper.RemoveAccents(result.localidade).Replace("'", "").ToUpper()}' and State eq '{address.State.ToUpper()}'").Result.FirstOrDefault();

                    if (county != null)
                    {
                        address.District = string.IsNullOrEmpty(result.bairro) ? address.District : result.bairro;
                        address.City = string.IsNullOrEmpty(result.localidade) ? address.City : result.localidade;
                        address.UF = string.IsNullOrEmpty(result.uf) ? address.UF : result.uf;
                    }
                }
            }
            //--------------------REPUBLICA VIRTUAL--------------------
            if (county == null)
            {
                var result = OrderFacade.ShippingRepo.GetAddressRepublicaVirtual(address.ZipCode);

                if (!string.IsNullOrEmpty(result.cidade))
                {
                    county = ServiceLayerXS.HttpClient.GetAsync<CountyModel>($"?$filter=Name eq '{result.cidade.ToUpper()}' and State eq '{result.uf.ToUpper()}'").Result.FirstOrDefault();

                    if (county == null)
                        county = ServiceLayerXS.HttpClient.GetAsync<CountyModel>($"?$filter=Name eq '{BusinessPartnerHelper.RemoveAccents(result.cidade).Replace("'", "").ToUpper()}' and State eq '{address.State.ToUpper()}'").Result.FirstOrDefault();

                    if (county != null)
                    {
                        address.District = string.IsNullOrEmpty(result.bairro) ? address.District : result.bairro;
                        address.City = string.IsNullOrEmpty(result.cidade) ? address.City : result.cidade;
                        address.UF = string.IsNullOrEmpty(result.uf) ? address.UF : result.uf;
                    }
                }
            }

            //--------------------CORREIOS--------------------
            if (county == null)
            {
                var result = OrderFacade.ShippingRepo.GetAddressCorreios(address.ZipCode);

                if (!string.IsNullOrEmpty(result.cidade))
                {
                    county = ServiceLayerXS.HttpClient.GetAsync<CountyModel>($"?$filter=Name eq '{result.cidade.ToUpper()}' and State eq '{result.uf.ToUpper()}'").Result.FirstOrDefault();

                    if (county == null)
                        county = ServiceLayerXS.HttpClient.GetAsync<CountyModel>($"?$filter=Name eq '{BusinessPartnerHelper.RemoveAccents(result.cidade).Replace("'", "").ToUpper()}' and State eq '{address.State.ToUpper()}'").Result.FirstOrDefault();

                    if (county != null)
                    {
                        address.District = string.IsNullOrEmpty(result.bairro) ? address.District : result.bairro;
                        address.City = string.IsNullOrEmpty(result.cidade) ? address.City : result.cidade;
                        address.UF = string.IsNullOrEmpty(result.uf) ? address.UF : result.uf;
                    }
                }
            }

            if (county is null)
                throw new Exception($"O CEP {address.ZipCode} é inválido.");

            return county.AbsId.ToString();
        }

    }
}
