﻿using Hangfire.Server;
using Models;
using Models.ServiceLayer.BusinessPartner;
using System.Threading.Tasks;

namespace Domain.BusinessPartner.Interfaces
{
    public interface IBusinessPartnerService
    {
        Task UpsertBusinessPartner(string OrderNumber, string SiteName, PerformContext context);

        void FillAddress(BusinessPartnerModel businessPartner, NewAddressModel address);
        void FillB2BFields(NewOrderModel orderModel, BusinessPartnerModel businessPartner);
        void FillFiscalData(BusinessPartnerModel businessPartner, NewCustomerModel customer);
        void FillPayment(BusinessPartnerModel businessPartner, NewPaymentModel payment);

        string GetCounty(NewAddressModel address);
    }
}