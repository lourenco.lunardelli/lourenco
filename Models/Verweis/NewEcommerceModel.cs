﻿using System.Collections.Generic;

namespace Models.Verweis
{
    public class NewEcommerceModel
    {
        public int EcommerceID { get; set; }
        public string Name { get; set; }

        public virtual NewCompanyModel Company { get; set; }
        public virtual NewPlatformModel Platform { get; set; }
    }
}
