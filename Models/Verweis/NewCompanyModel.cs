﻿namespace Models.Verweis
{
    public class NewCompanyModel
    {
        public int CompanyID { get; set; }
        public string Name { get; set; }
    }
}
