﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Verweis
{
    public class NewPlatformModel
    {
        public int PlatformID { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
    }
}
