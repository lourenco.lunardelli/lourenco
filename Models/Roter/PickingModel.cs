﻿namespace Models.Roter
{
    public class PickingModel
    {
        public PickingItemModel[] picking { get; set; }
    }

    public class PickingItemModel
    {
        public int batchEntry { get; set; }
        public string batchNumber { get; set; }
        public string binCode { get; set; }
        public int binEntry { get; set; }
        public string itemCode { get; set; }
        public int orderLine { get; set; }
        public int quantity { get; set; }
        public int stockOutQuantity { get; set; }
        public int oldBinEntry { get; set; }
        public int newBinEntry { get; set; }
    }
}