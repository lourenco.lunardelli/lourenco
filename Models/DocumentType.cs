﻿namespace Models
{
    public class DocumentType
    {
        public int DocumentTypeID { get; set; }
        public string Name { get; set; }
    }
}
