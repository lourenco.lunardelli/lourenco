﻿using System;

namespace Models
{
    public class NewPaymentModel
    {
        public int PaymentID { get; set; }
        public string PaymentMethodID { get; set; }
        public double Total { get; set; }
        public double Discount { get; set; }
        public int Installments { get; set; }
        public DateTime DueDate { get; set; }
        public string Currency { get; set; }
        public string TransactionCode { get; set; }

        public virtual NewAddressModel Address { get; set; }
    }
}
