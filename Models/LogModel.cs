﻿namespace Models
{
    public class LogModel
    {
        public int LogID { get; set; }
        public string Document { get; set; }
        public string Message { get; set; }


        public virtual NewOrderModel Order { get; set; }
        public virtual LogTypeModel LogType { get; set; }
    }
}
