﻿namespace Models
{
    public class ResselerInfo
    {
        public int ResselerInfoID { get; set; }
        public string StateRegistry { get; set; }
        public string CorporateName { get; set; }
        public string SellerName { get; set; }
    }
}
