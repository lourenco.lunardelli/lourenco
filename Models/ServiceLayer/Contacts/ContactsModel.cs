﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.ServiceLayer.Contacts
{
    public class ContactsModel
    {
        public string Name { get; set; }
        public string Phone1 { get; set; }
        public string E_Mail { get; set; }
        public string Active { get; set; }
        public string FirstName { get; set; }
        public string U_BCECPF { get; set; }
        public string U_BCESkype { get; set; }
        public string U_BCEPodeCotar { get; set; }
        public string U_BCEPodeComprar { get; set; }
        public string U_BCEPodeRetirar { get; set; }
        public int InternalCode { get; set; }
    }
}
