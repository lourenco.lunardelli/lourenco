﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.ServiceLayer.Orders
{
    public class TaxExtensionModel
    {
        public int? MainUsage { get; set; }
        public int? PackQuantity { get; set; }
        public string Carrier { get; set; }
        public string Incoterms { get; set; }
    }
}
