﻿using System;
using System.Collections.Generic;

namespace Models.ServiceLayer.Orders
{
    public class SAPOrderModel
    {
        public string DocNum { get; set; }
        public DateTime? DocDueDate { get; set; }
        public string CardCode { get; set; }
        public string U_SHL_IDPedido { get; set; }
        public int? BPL_IDAssignedToInvoice { get; set; }
        public int? SalesPersonCode { get; set; }
        public int TransportationCode { get; set; }
        public string Comments { get; set; }
        public string ClosingRemarks { get; set; }
        public double? DocTotal { get; set; }

        //----------PAYMENT
        public string PaymentMethod { get; set; }
        public int? PaymentGroupCode { get; set; }

        //----------USER FIELDS
        public string U_SKILL_FormaPagto { get; set; }
        public string U_Status_WMS { get; set; }
        public double U_ICMS_ST { get; set; }
        public double U_BCE_IPI { get; set; }
        public string U_ERP_StatusSHL { get; set; }
        public string U_ERP_TipoPedido { get; set; }
        public string U_ERP_TipoSHL { get; set; }
        public string U_MONE_CHANNEL { get; set; }
        public string U_ID_Marketplace { get; set; }
        public string U_Site { get; set; }
        public string U_G2W_TipoPV { get; set; }


        public TaxExtensionModel TaxExtension { get; set; }
        public List<AdditionalExpenseModel> DocumentAdditionalExpenses { get; set; }

        //PRODUCTS
        public List<DocumentLineModel> DocumentLines { get; set; }
    }
}


