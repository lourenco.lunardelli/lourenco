﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Models.ServiceLayer.Documents
{
    public class SAPDocuments
    {
        public string CardCode { get; set; }

        public int? OrderEntry { get; set; }

        public int? AREntry { get; set; }

        public int? APEntry { get; set; }

        public int? PickingListEntry { get; set; }

        public int? InvoiceEntry { get; set; }
        public DateTime? InvoiceDate { get; set; }

        public int? IncomingPaymentEntry { get; set; }

        public string METracking { get; set; }

        public DateTime OrderDueDate { get; set; }

        public string isCanceled { get; set; }

        public string OrderNumber { get; set; }

        public string U_Status_WMS { get; set; }
    }
}