﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.ServiceLayer.Shippings
{
    public class METrackingModel
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public string U_Tracking { get; set; }
        public int U_OrderEntry { get; set; }
        public string U_Status { get; set; }
        public string U_TrackingRangeCode { get; set; }
        public string U_PLP { get; set; }
        public string U_ExtNF { get; set; }
        public int U_ShippingTypeCode { get; set; }
        public int? U_InvoiceEntry { get; set; }
    }
}
