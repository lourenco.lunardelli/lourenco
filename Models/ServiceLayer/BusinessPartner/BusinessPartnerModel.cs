﻿using Models.ServiceLayer.Contacts;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Models.ServiceLayer.BusinessPartner
{
    public class BusinessPartnerModel
    {
        public string CardCode { get; set; }
        public string CardName { get; set; }
        public string CardType { get; set; }
        public int? GroupCode { get; set; }   
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string Currency { get; set; }       
        public string EmailAddress { get; set; }
        public string Password { get; set; }
        public string ShipToDefault { get; set; }
        public string BilltoDefault { get; set; }
        public int LanguageCode { get; set; }
        public int Series { get; set; }
        public string Cellular { get; set; }
        public string AliasName { get; set; }
        public string PeymentMethodCode { get; set; }


        public List<BusinessPartnerPaymentMethods> BPPaymentMethods { get; set; }
        public List<BusinessPartnerAddress> BPAddresses { get; set; }
        public List<BusinessPartnerFiscalTaxIdCollection> BPFiscalTaxIDCollection { get; set; }

        [IgnoreDataMember]
        public List<ContactsModel> ContactEmployees { get; set; }

        //-----------------------USER FIELDS-----------------------
        public DateTime U_IE_UltimaBusca { get; set; }
        public string U_ERP_MagCode { get; set; }
        public string U_ERP_LISTA_PN { get; set; }
        public string U_BCEEmployeesNo { get; set; }
        public string U_BCEWebSite { get; set; }
        public string U_BCEOwnHeadquarter { get; set; }
        public DateTime? U_BCEFoundationDate { get; set; }
        public string U_BCEAbout { get; set; }
        
    }
}