﻿using Newtonsoft.Json;

namespace Models.ServiceLayer
{
    public class ServiceLayerList<T>
    {
        [JsonProperty("odata.count")]
        public long? Count;
        [JsonProperty("odatametadata")]
        public string ODataMetadata { get; set; }
        [JsonProperty("odata.nextLink")]
        public string NextLink { get; set; }
        public T Value { get; set; }
    }
}
