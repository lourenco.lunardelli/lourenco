﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.ServiceLayer.Item
{
    public class ItemModel
    {
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public int ItmsGrpCod { get; set; }
        public string SellItem { get; set; }
        public string InvntItem { get; set; }
        public int OnHand { get; set; }
        public int IsCommited { get; set; }
        public int OnOrder { get; set; }
        public string MaxLevel { get; set; }
        public string MinLevel { get; set; }
        public string U_GarantiaItens { get; set; }

        public List<ItemWarehouseInfo> ItemWarehouseInfoCollection { get; set; }
    }

    public class ItemWarehouseInfo
    {
        public double StandardAveragePrice { get; set; }
        public string WarehouseCode { get; set; }
    }
}
