﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Models.ServiceLayer
{
    public class SLRoot<T>
    {
        [JsonProperty("odata.count")]
        public long? count;

        [JsonProperty("value")]
        public List<T> Values { get; set; }

        [JsonProperty("odata.nextLink")]
        public string QueryNext { get; set; }
    }
}
